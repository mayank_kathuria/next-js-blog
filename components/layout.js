import Styles from './layout.module.css';

function Layout({ children }) {
    return <div className={Styles.container}>{children}</div>
  }
  
  export default Layout;